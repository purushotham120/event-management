import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showbirthday',
  templateUrl: './showbirthday.component.html',
  styleUrls: ['./showbirthday.component.css']
})
export class ShowbirthdayComponent implements OnInit {

  events: any;

  constructor(private router: Router) {
    this.events = [
      {name:"birthday", description:"birthday parties",imgsrc:"assets/45.jpg"}
    ];
  }

  ngOnInit() {

  }

}
