import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowbirthdayComponent } from './showbirthday.component';

describe('ShowbirthdayComponent', () => {
  let component: ShowbirthdayComponent;
  let fixture: ComponentFixture<ShowbirthdayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowbirthdayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowbirthdayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
