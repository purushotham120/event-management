import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showcorporate',
  templateUrl: './showcorporate.component.html',
  styleUrls: ['./showcorporate.component.css']
})
export class ShowcorporateComponent implements OnInit {
  events: any;

  constructor(private router: Router)  {
    this.events = [
      {name:"corporate", description:"corporate events", imgsrc:"../../assets/CECRSL-1.jpg"}
    ];

  }
  ngOnInit() {

  }

}
