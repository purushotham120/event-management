import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcorporateComponent } from './showcorporate.component';

describe('ShowcorporateComponent', () => {
  let component: ShowcorporateComponent;
  let fixture: ComponentFixture<ShowcorporateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowcorporateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowcorporateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
