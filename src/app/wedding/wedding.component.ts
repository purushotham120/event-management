import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wedding',
  templateUrl: './wedding.component.html',
  styleUrls: ['./wedding.component.css']
})
export class WeddingComponent implements OnInit {
  events: any;

  constructor(private router: Router){
    this.events = [
      {name:"Anniversary", description:"Weddings mark the start of a new chapter in a couple's life and celebrate their love and commitment to each other",imgsrc:"assets/brigade1.jpg"},
     {name:"Birthday", description:"Birthday parties are celebrations held to mark the anniversary of a person's birth, involving gifts, cake, food, and socializing with friends and ",imgsrc:"assets/22.jpg"},
    {name:"media planner", description:"entertainment",imgsrc:"assets/81.jpg"},
      {name:"corporate events", description:"sfx",imgsrc:"assets/82.jpg"},

    ];
  }
  ngOnInit() {

  }
  ReadMore() {
    this.router.navigateByUrl('/showwedding');
  }

  }
