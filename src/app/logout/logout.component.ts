import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private service: EventService, private router: Router) {
    this.service.setUserLogout();
    this.router.navigate(['login']);
  }
  ngOnInit() {

  }

}
