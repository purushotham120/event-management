import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WeddingComponent } from './wedding/wedding.component';
import { ShowweddingComponent } from './showwedding/showwedding.component';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { SportseventComponent } from './sportsevent/sportsevent.component';
import { ShowsportseventComponent } from './showsportsevent/showsportsevent.component';
import { ToastrModule } from 'ngx-toastr';
import { BirthdayComponent } from './birthday/birthday.component';
import { ShowbirthdayComponent } from './showbirthday/showbirthday.component';
import { UniqueComponent } from './unique/unique.component';
import { ShowuniqueComponent } from './showunique/showunique.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ShowmediaComponent } from './showmedia/showmedia.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ShowcorporateComponent } from './showcorporate/showcorporate.component';
import { ShowcelebrityComponent } from './showcelebrity/showcelebrity.component';
import { ShowaboutComponent } from './showabout/showabout.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './auth.guard';
import { ProtectedComponent } from './protected.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WeddingComponent,
    ShowweddingComponent,
    RegisterComponent,
    SportseventComponent,
    ShowsportseventComponent,
    BirthdayComponent,
    ShowbirthdayComponent,
    UniqueComponent,
    ShowuniqueComponent,
    HomepageComponent,
    ShowmediaComponent,
    NavbarComponent,
    ShowcorporateComponent,
    ShowcelebrityComponent,
    ShowaboutComponent,
    LogoutComponent,
    ProtectedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
