import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowweddingComponent } from './showwedding/showwedding.component';
import { WeddingComponent } from './wedding/wedding.component';
import { SportseventComponent } from './sportsevent/sportsevent.component';
import { ShowsportseventComponent } from './showsportsevent/showsportsevent.component';
import { RegisterComponent } from './register/register.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { ShowbirthdayComponent } from './showbirthday/showbirthday.component';
import { UniqueComponent } from './unique/unique.component';
import { ShowuniqueComponent } from './showunique/showunique.component';
import { ShowmediaComponent } from './showmedia/showmedia.component';
import { ShowcorporateComponent } from './showcorporate/showcorporate.component';
import { ShowcelebrityComponent } from './showcelebrity/showcelebrity.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ShowaboutComponent } from './showabout/showabout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './auth.guard';
import { ProtectedComponent } from './protected.component';


const routes: Routes = [
{path:'', component:HomepageComponent},
{path:'showwedding', component:ShowweddingComponent},
{path:'wedding', component:WeddingComponent},
{path:'sportsevent',component:SportseventComponent},
{path:'showsportsevent',component:ShowsportseventComponent},
{path:'register',component:RegisterComponent},
{path:'birthday',component:BirthdayComponent},
{path:'showbirthday',component:ShowbirthdayComponent},
{path:'unique',component:UniqueComponent},
{path:'showunique',component:ShowuniqueComponent},
{path:'showmedia',component:ShowmediaComponent},
{path:'showcorporate',component:ShowcorporateComponent},
{path:'showcelebrity',component:ShowcelebrityComponent},
{path:'showabout',component:ShowaboutComponent},
{path:'navbar',component:NavbarComponent},
{path:'logout',component:LogoutComponent},
{path: 'protected',component:ProtectedComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
