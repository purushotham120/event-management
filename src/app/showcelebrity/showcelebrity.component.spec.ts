import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcelebrityComponent } from './showcelebrity.component';

describe('ShowcelebrityComponent', () => {
  let component: ShowcelebrityComponent;
  let fixture: ComponentFixture<ShowcelebrityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowcelebrityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowcelebrityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
