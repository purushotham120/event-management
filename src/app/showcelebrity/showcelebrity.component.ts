import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showcelebrity',
  templateUrl: './showcelebrity.component.html',
  styleUrls: ['./showcelebrity.component.css']
})
export class ShowcelebrityComponent implements OnInit {
  events: any;

  constructor(private router: Router)  {
    this.events = [
      {name:"celebrity", description:"celebrity management", imgsrc:"../../assets/CLBT1.jpg"}
    ];

  }
  ngOnInit() {

  }

}
