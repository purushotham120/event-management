import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EventService } from '../event.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: any;

  constructor(
    private formBuilder: FormBuilder,
    private eventService: EventService,
    private toastr: ToastrService,
    private http: HttpClient,
    registerForm: FormGroup
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      FirstName: ['', [Validators.required, Validators.minLength(2), Validators.pattern("[a-zA-Z].*")]],
      LastName: ['', [Validators.required, Validators.minLength(2), Validators.pattern("[a-zA-Z].*")]],
      MobileNumber: ['', [Validators.required, Validators.pattern("[0-9]+$"), Validators.minLength(10), Validators.maxLength(10)]],
      emailId: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      ConfirmPassword: ['', [Validators.minLength(6),Validators.maxLength(20)]],
      Gender: ['', [Validators.required]]
    });
  }

  get FirstName(){
    return this.registerForm.controls.firstname;
  }

  get LastName() {
    return this.registerForm.controls.lastname;
  }

  get Gender() {
    return this.registerForm.controls.gender;
  }

  get MobileNumber() {
    return this.registerForm.controls.mobilenumber;
  }

  get EmailId() {
    return this.registerForm.controls.emailId;
  }

  get Password() {
    return this.registerForm.controls.Password;
  }

  get ConfirmPassword() {
    return this.registerForm.controls.ConfirmPassword;
  }

  registerSubmitted() {
    if (this.registerForm.valid) {
      //Process the registration form submission
      this.toastr.success('Registration successful!','success');
      console.log(this.registerForm.value);
    } else {
      //Handle form validation errors
      this.toastr.error('Please fill in all required fields correctly','error');
      this.registerForm.marksAllAsTouched();
    }
}

}
