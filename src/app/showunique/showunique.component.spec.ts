import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowuniqueComponent } from './showunique.component';

describe('ShowuniqueComponent', () => {
  let component: ShowuniqueComponent;
  let fixture: ComponentFixture<ShowuniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowuniqueComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowuniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
