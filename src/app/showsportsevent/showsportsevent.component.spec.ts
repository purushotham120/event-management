import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowsportseventComponent } from './showsportsevent.component';

describe('ShowsportseventComponent', () => {
  let component: ShowsportseventComponent;
  let fixture: ComponentFixture<ShowsportseventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowsportseventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowsportseventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
