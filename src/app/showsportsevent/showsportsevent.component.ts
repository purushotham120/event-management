import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showsportsevent',
  templateUrl: './showsportsevent.component.html',
  styleUrls: ['./showsportsevent.component.css']
})
export class ShowsportseventComponent implements OnInit {
  events: any;

  constructor(private router: Router) {
    this.events = [
      {name:"sportsevent",description:"sports",imgsrc:"/assets/23.jpg"}

    ];
  }
  ngOnInit() {

  }

}
