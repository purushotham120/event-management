import { Component, OnInit } from '@angular/core';

import { Route, Router } from '@angular/router';
@Component({
  selector: 'app-showwedding',
  templateUrl: './showwedding.component.html',
  styleUrls: ['./showwedding.component.css']
})
export class ShowweddingComponent implements OnInit {
event: any;


 constructor(private router: Router) {
  this.event = [
    {name:"Wedding & Receptions",description:"A Wedding is a marriage ceremony where legally or formally recognized union of two people as partners in a personal relationship. Wedding conventions and traditions shift incredibly between societies, ethnic gatherings, religions, nations, and social classes. Most wedding functions include a trade of marriage promises by the couple, introduction of blessing offering, ring representative thing, blossoms, cash, and an open announcement of marriage by a specialist figure", imgsrc:"/assets/91.jpg"},
  ];

 }
  ngOnInit() {

  }

}
