import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowweddingComponent } from './showwedding.component';

describe('ShowweddingComponent', () => {
  let component: ShowweddingComponent;
  let fixture: ComponentFixture<ShowweddingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowweddingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowweddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
