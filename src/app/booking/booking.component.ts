import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { EventservService } from 'src/app/eventserv.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  bookingDate: string;
  currentDate: Date;
  constructor(private service: EventservService, private router: Router, private toaster: NgToastService) {
    this.bookingDate='';
    this.currentDate = new Date();
  }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
  advanceDate() {
    this.currentDate.setDate(this.currentDate.getDate() + 1);
  }

  BookingFormSubmit(bookingForm: any) {
    this.service.addBooking(bookingForm).subscribe();
    this.toaster.success({ detail: "Booking Successfully", duration: 5000 });
    this.router.navigate(['home']);
  }


}
