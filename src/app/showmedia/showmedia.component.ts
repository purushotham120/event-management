import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showmedia',
  templateUrl: './showmedia.component.html',
  styleUrls: ['./showmedia.component.css']
})
export class ShowmediaComponent implements OnInit {
  events: any;

  constructor(private router: Router)  {
    this.events = [
      {name:"Media", description:"MediaPlanner",imgsrc:"../../assets/MD.jpg"}
    ];

  }
  ngOnInit() {

  }

}
