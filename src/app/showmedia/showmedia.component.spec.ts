import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowmediaComponent } from './showmedia.component';

describe('ShowmediaComponent', () => {
  let component: ShowmediaComponent;
  let fixture: ComponentFixture<ShowmediaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowmediaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowmediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
