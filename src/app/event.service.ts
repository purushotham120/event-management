import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private baseUrl = 'your_base_url_here';
  private isUserLogged: boolean;
  private LoginStatus: Subject<any>;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.LoginStatus = new Subject();
  }

  getLoginStatus(): Observable<any> {
    return this.LoginStatus.asObservable();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.LoginStatus.next(true);
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  setUserLogout() {
    this.isUserLogged = false;
    this.LoginStatus.next(false);
  }

  getAllevents(): Observable<any> {
    return this.http.get(`${this.baseUrl}/getAllevents`);
  }

  geteventbyid(eventId: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/geteventbyId/${eventId}`);
  }

  getevent(loginform: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/login/${loginform.emailId}/${loginform.password}`);
  }

  deleteevent(eventId: any): Observable<any> {
    return this.http.delete(`${this.baseUrl}/deleteeventbyId/${eventId}`);
  }

  registerevent(event: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/registerevent`, event);
  }

  updateevent(event: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/updateevent`, event);
  }
  registerUser(registrationData: any): Observable<any> {
    //Register the user and  return the observble
    //Example:
    return this.http.post<any>('your-api-endpoint', registrationData);
  }
}
