import { Injectable, Component } from '@angular/core';
import { CanActivate, Router, Routes } from '@angular/router';
import { EventService } from './event.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthGuard,
  ) { }

  canActivate(): boolean {
    if (this.authService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  isLoggedIn(): boolean {
    // Logic to check if the user is logged in
    // Return true if logged in, false otherwise
    return true; // Replace with your actual logic
  }


}
