import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from '../event.service';
import { Toast, ToastrComponentlessModule, ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  getData: boolean = false;


  constructor(private router: Router, private eventService: EventService, private toastr: ToastrService) {}

  ngOnInit() {}

  async login(loginForm: any) {
    console.log(loginForm);
  }

  loginUser() {
    var username = this.model.username;
    var password = this.model.password;

    console.log(username + " " + password);

    this.eventService.getevent(this.model).subscribe((res: any) => {
      this.getData = res;

      if (this.getData) {
        this.eventService.setUserLoggedIn();
        this.router.navigate(["/home/"]);
      } else {
        this.toastr.error("Invalid Username/password");
      }
    });
  }
}
