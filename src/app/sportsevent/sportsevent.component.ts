import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sportsevent',
  templateUrl: './sportsevent.component.html',
  styleUrls: ['./sportsevent.component.css']
})
export class SportseventComponent implements OnInit {
events: any;

  constructor(private router: Router) {
    this.events = [
      {name:"sportsevent", description:"sports", imgsrc:"assets/22.jpg"}

    ];

  }
  ngOnInit() {

  }
  // READMORE() {
  //   this.router.navigateByUrl('/showsportsevent');
  // }

}
