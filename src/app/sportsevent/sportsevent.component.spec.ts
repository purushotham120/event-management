import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportseventComponent } from './sportsevent.component';

describe('SportseventComponent', () => {
  let component: SportseventComponent;
  let fixture: ComponentFixture<SportseventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportseventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SportseventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
