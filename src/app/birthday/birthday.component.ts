import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.css']
})
export class BirthdayComponent implements OnInit {
  events: any;

  constructor(private router: Router) {
    this.events = [
      { name: "birthday", description:"Birthday parties are celebrations held to mark the anniversary of a person's birth, involving gifts, cake, food..", imgsrc:"assets/44.jpg"}
    ];
  }

  ngOnInit() {

  }


}
